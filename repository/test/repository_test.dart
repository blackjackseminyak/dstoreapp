import 'package:test/test.dart';
import 'package:repository/repository.dart';

// Refer https://www.dartdocs.org/documentation/matcher/latest/matcher/matcher-library.html
void main() {
  test('create a mock dStore entity', () {
    final dStoreEntity = new DStoreEntity(
        'Wannabe Inc',
        'f47ac10b-58cc-4372-a567-0e02b2c3d479',
        'Singapore',
        ['79','Ayer Rajah Crescent'],
        'Singapore',
        'SG',
        '121223',
        '(+65)60 921 5150',
        'chris@wannabeinc.com',
        'Business Services',
        Uri.parse('www.wannabeinc.com'),
        false,
        false
    );    
       
    expect(dStoreEntity, isNotNull);
    expect(dStoreEntity, new isInstanceOf<DStoreEntity>());
    expect(dStoreEntity.name, equals("Wannabe Inc"));
    expect(dStoreEntity.id, equals("f47ac10b-58cc-4372-a567-0e02b2c3d479"));
    expect(dStoreEntity.country, equals("Singapore"));
    expect(dStoreEntity.streetAddress, hasLength(2));
    expect(dStoreEntity.city, equals("Singapore"));
    expect(dStoreEntity.state, equals("SG"));
    expect(dStoreEntity.pincode, equals("121223"));
    expect(dStoreEntity.email, equals("chris@wannabeinc.com"));
    expect(dStoreEntity.category, equals("Business Services"));
    expect(dStoreEntity.website, equals("www.wannabeinc.com"));
    expect(dStoreEntity.isHomeDelivered, isFalse);
    expect(dStoreEntity.publish, isFalse);
    //expect(() => calculator.addOne(null), throwsNoSuchMethodError);
    
    expect(dStoreEntity.toJson(), isMap);
    expect(dStoreEntity.toJson(), returnsNormally);
    
  });
  
  test('anything', () {
    var obj = new DStoreEntity('','','',['',''],'','','','','','',Uri.parse('www.anything.com'),false,false);
    
    expect(obj, anything);
    
    dynamic reSerialized = DStoreEntity.fromJson(obj.toJson());
    
    expect(reSerialized, new isInstanceOf<DStoreEntity>());
    
  });
}

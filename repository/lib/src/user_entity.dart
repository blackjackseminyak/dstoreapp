/// (UserEntity) model class for user of dstore android app
class UserEntity {
  final String id;
  final String displayName;
  final String photoUrl;

  /// Constructor (UserEntity)
  UserEntity({this.id, this.displayName, this.photoUrl});

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserEntity &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          displayName == other.displayName &&
          photoUrl == other.photoUrl;

  /// (UserEntity) class overrides (hashCode) getter property  of (Object) type
  @override
  int get hashCode => id.hashCode ^ displayName.hashCode ^ photoUrl.hashCode;

  /// (Object) method (toString) is overriden for (UserEntity) class
  @override
  String toString() {
    return 'UserEntity{id: $id, displayName: $displayName, photoUrl: $photoUrl}';
  }
}

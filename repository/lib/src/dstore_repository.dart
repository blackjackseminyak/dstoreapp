import 'dart:async';
import 'dart:core';

import 'dstore_entity.dart';

/// A class that Loads and Persists dstore. The data layer of the app.
///
/// How and where it stores the entities should defined in a concrete
/// implementation, such as dstore_repository_firebase or dstore_repository_web.
///
/// The domain layer should depend on this abstract class, and each app can
/// inject the correct implementation depending on the environment, such as
/// web, local or Firebase.
abstract class DStoreRepository {
  /// Loads dstore first from File storage. If they don't exist or encounter an
  /// error, it attempts to load the mstore from a Firebase Client.
  Future<DStoreEntity> loadDStore();

  // Persists dstore to local disk and the web
  Future saveDStore(DStoreEntity dstore);
}

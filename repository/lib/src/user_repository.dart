import 'dart:async';
import 'user_entity.dart';

/// A class that Loads and Persists UserEntity. The data layer of the UserEntity.
///
/// How and where it stores the entities should defined in a concrete
/// implementation, such as user_repository_firebase or user_repository_web.
///
/// The domain layer should depend on this abstract class, and each app can
/// inject the correct implementation depending on the environment, such as
/// web, local or Firebase.
abstract class UserRepository {
  Future<UserEntity> login();
}

import 'uuid.dart';
import 'package:repository/repository.dart';

class AppState {
  bool isLoading;
  DStore dstore;

  AppState({
    this.isLoading = false,
    this.dstore,
  });

  factory AppState.loading() => new AppState(isLoading: true);

  bool get storePublished => (dstore.publish);

  @override
  int get hashCode => dstore.hashCode ^ isLoading.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppState &&
          runtimeType == other.runtimeType &&
          dstore == other.dstore &&
          isLoading == other.isLoading;

  void publishStore() {
    dstore.publish = true;
  }

  @override
  String toString() {
    return 'AppState{dstore: $dstore, isLoading: $isLoading}';
  }
}

class DStore {
  bool publish;
  String id;
  String name;
  String country;
  List<String> streetAddress;
  String city;
  String state;
  String pincode;
  String phone;
  String email;
  String category;
  Uri website;
  bool isHomeDelivered;

  DStore(this.name,
      {this.publish = false,
      this.country = '',
      this.streetAddress = const ['', ''],
      this.city = '',
      this.state = '',
      this.pincode = '',
      this.phone = '',
      this.email = '',
      this.category = '',
      this.website,
      this.isHomeDelivered = false,
      String id})
      : this.id = id ?? new Uuid().generateV4();

  @override
  @override
  int get hashCode =>
      publish.hashCode ^
      name.hashCode ^
      country.hashCode ^
      streetAddress.hashCode ^
      city.hashCode ^
      state.hashCode ^
      pincode.hashCode ^
      phone.hashCode ^
      email.hashCode ^
      category.hashCode ^
      website.hashCode ^
      isHomeDelivered.hashCode ^
      id.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DStoreEntity &&
          runtimeType == other.runtimeType &&
          publish == other.publish &&
          name == other.name &&
          country == other.country &&
          streetAddress == other.streetAddress &&
          city == other.city &&
          state == other.state &&
          pincode == other.pincode &&
          email == other.email &&
          category == other.category &&
          website == other.website &&
          isHomeDelivered == other.isHomeDelivered &&
          id == other.id;
  @override
  @override
  String toString() {
    return '''
            DStoreEntity{publish: $publish, 
            name: $name, 
            country: $country, 
            streetAddress: $streetAddress, 
            city: $city, 
            state: $state, 
            pincode: $pincode, 
            phone: $phone, 
            email: $email, 
            category: $category, 
            website: $website, 
            homedelivered: $isHomeDelivered, 
            id: $id}
            ''';
  }

  DStoreEntity toEntity() {
    return new DStoreEntity(name, id, country, streetAddress, city, state,
        pincode, phone, email, category, website, isHomeDelivered, publish);
  }

  static DStore fromEntity(DStoreEntity entity) {
    return new DStore(
      entity.name,
      publish: entity.publish ?? false,
      country: entity.country,
      streetAddress: entity.streetAddress,
      city: entity.city,
      state: entity.state,
      pincode: entity.pincode,
      phone: entity.phone,
      email: entity.email,
      category: entity.category,
      website: entity.website,
      isHomeDelivered: entity.isHomeDelivered ?? false,
      id: entity.id ?? new Uuid().generateV4(),
    );
  }
}

class Vendor {
  const Vendor({
    this.name,
    this.description,
    this.avatarAsset,
    this.avatarAssetPackage,
  });

  final String name;
  final String description;
  final String avatarAsset;
  final String avatarAssetPackage;

  bool isValid() {
    return name != null && description != null && avatarAsset != null;
  }

  @override
  String toString() => 'Vendor($name)';
}

class Product {
  const Product(
      {this.name,
      this.description,
      this.featureTitle,
      this.featureDescription,
      this.imageAsset,
      this.imageAssetPackage,
      this.categories,
      this.price,
      this.vendor});

  final String name;
  final String description;
  final String featureTitle;
  final String featureDescription;
  final String imageAsset;
  final String imageAssetPackage;
  final List<String> categories;
  final double price;
  final Vendor vendor;

  String get tag => name; // Unique value for Heroes
  String get priceString => '\$${price.floor()}';

  bool isValid() {
    return name != null &&
        description != null &&
        imageAsset != null &&
        categories != null &&
        categories.isNotEmpty &&
        price != null &&
        vendor.isValid();
  }

  @override
  String toString() => 'Product($name)';
}

class ProductPhoto {
  ProductPhoto({
    this.assetName,
    this.assetPackage,
    this.title,
    this.caption,
    this.price,
    this.isFavorite: false,
  });

  final String assetName;
  final String assetPackage;
  final String title;
  final String caption;
  final double price;

  bool isFavorite;
  String get tag => assetName; // Assuming that all asset names are unique.

  bool get isValid =>
      assetName != null &&
      title != null &&
      caption != null &&
      price != null &&
      isFavorite != null;
}
